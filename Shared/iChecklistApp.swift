//
//  iChecklistApp.swift
//  Shared
//
//  Created by Maciej Zajęcki on 16/10/2021.
//

import SwiftUI

@main
struct iChecklistApp: App {
    @StateObject private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            Checklists()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
