//
//  TextAlert.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import Foundation
import SwiftUI

public struct TextAlert {
  public var title: String
  public var message: String
  public var placeholder: String = ""
  public var accept: String = "OK"
  public var cancel: String? = "Cancel"
  public var secondaryActionTitle: String? = nil
  public var keyboardType: UIKeyboardType = .default
  public var action: (String?) -> Void
  public var secondaryAction: (() -> Void)? = nil
}
