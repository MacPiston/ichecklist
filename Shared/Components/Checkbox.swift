//
//  CheckboxView.swift
//  iChecklist
//
//  Created by Maciej Zajęcki on 16/10/2021.
//

import SwiftUI

struct Checkbox: View {
    @Binding var checked: Bool

    var body: some View {
        Image(systemName: checked ? "checkmark.square.fill" : "square")
            .resizable()
            .frame(width: 60, height: 60, alignment: .center)
            .foregroundColor(checked ? Color.green : Color.secondary)
            .onTapGesture {
                self.checked.toggle()
            }
    }
}

struct Checkbox_Previews: PreviewProvider {
    struct CheckboxViewHolder: View {
        @State var checked = true

        var body: some View {
            Checkbox(checked: $checked)
        }
    }

    static var previews: some View {
        CheckboxViewHolder()
    }
}
