//
//  DataController.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import Foundation
import CoreData
import SwiftUI

class DataController: ObservableObject {
    let container = NSPersistentContainer(name: "iChecklist")
    
    init() {
        container.loadPersistentStores(completionHandler: { description, error in
            if let error = error {
                print("Failed to load CoreData: \(error.localizedDescription)")
            }
        })
    }
}
