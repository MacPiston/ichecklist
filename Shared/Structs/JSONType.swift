//
//  JsonData.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import Foundation

struct JSONChecklistItem: Codable {
    let action: String
    let step: String
}

struct JSONChecklist: Codable {
    let isEmergency: Bool
    let name: String
    let items: [JSONChecklistItem]
}

struct JSONType: Codable {
    let name: String
    let checklists: [JSONChecklist]
}
