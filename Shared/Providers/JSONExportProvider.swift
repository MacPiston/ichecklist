//
//  JSONExportProvider.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import Foundation
import SwiftUI
import CoreData

struct JSONExportProvider {
    private let encoder: JSONEncoder = JSONEncoder()
    
    private func dataToArray(data: [TypeEntity]) -> [JSONType] {
        let jsonTypes: [JSONType] = data.map({ type in
            return JSONType(name: type.name!, checklists: type.checklists!.map({ checklist in
                let checklist = checklist as! ChecklistEntity
                
                return JSONChecklist(isEmergency: checklist.isEmergency, name: checklist.name!, items: checklist.items!.map({ item in
                    let item = item as! ChecklistItemEntity
                    return JSONChecklistItem(action: item.action!, step: item.step!)
                }))
            }))
        })
        
        return jsonTypes
    }
    
    @discardableResult
    private func shareToFile(jsonString: String) -> Bool {
        guard let source = UIApplication.shared.windows.last?.rootViewController else {
            return false
        }
                
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-YY"
        let dateString = formatter.string(from: Date())
        print(dateString)
        
        let data = jsonString.data(using: .utf8)
        let fileURL = data?.dataToFile(fileName: String(format: "iChecklist_export_%@.json", dateString))
        var files = [Any]()
        
        files.append(fileURL!)
        
        let activityVC = UIActivityViewController(activityItems: files, applicationActivities: nil)
        source.present(activityVC, animated: true)
        return true
    }
    
    func exportToJSON(data: [TypeEntity]) {
        let jsonEncodedData = dataToArray(data: data)
        
        do {
            let data = try encoder.encode(jsonEncodedData)
            let jsonString = String(data: data, encoding: .utf8)!
            print(jsonString)
            shareToFile(jsonString: jsonString)
            
        } catch {
            print("Error during exporting")
        }
    }
}
