//
//  TTSProvider.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import Foundation
import AVFoundation

struct TTSProvider {
    private static let defaultLanguage = "pl-PL"
    private let langStr = "pl-PL"
    private let synth = AVSpeechSynthesizer()
    private let presets = TTSPresets()
    
    func readItem(item: ChecklistItemEntity) {
        let currentPreset = presets.presets[langStr]
        
        let ttsString = String(format: "%s %s %s %s", currentPreset!.isString, item.step!, currentPreset!.checkString, item.action!)
        let utterance = AVSpeechUtterance(string: ttsString)
        utterance.voice = AVSpeechSynthesisVoice(language: langStr)
        
        synth.speak(utterance)
    }
}
