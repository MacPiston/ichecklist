//
//  TTSPresets.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import Foundation

struct TTSPresets {
    struct TTSPreset {
        let checkString: String
        let isString: String
    }
    
    let presets: [String: TTSPreset] = ["en-US": TTSPreset(checkString: "check", isString: "is"), "pl-PL": TTSPreset(checkString: "sprawdź", isString: "jest")]
}
