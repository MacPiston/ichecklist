//
//  JSONImportProvider.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import Foundation
import CoreData

struct JSONImportProvider {
    private let decoder: JSONDecoder = JSONDecoder()
    
    func importJson(fileUrl: URL, moc: NSManagedObjectContext) throws {
        let jsonData = try Data(contentsOf: fileUrl)
        
        let typesFromJson = try decoder.decode([JSONType].self, from: jsonData)
        
        typesFromJson.forEach({ jsonType in
            let newType = TypeEntity(context: moc)
            newType.id = UUID()
            newType.name = jsonType.name
            
            newType.checklists = NSSet(array: jsonType.checklists.map({ jsonChecklist in
                let newChecklist = ChecklistEntity(context: moc)
                newChecklist.id = UUID()
                newChecklist.isEmergency = jsonChecklist.isEmergency
                newChecklist.name = jsonChecklist.name
                newChecklist.type = newType
                newChecklist.items = NSSet(array: jsonChecklist.items.map({ jsonItem in
                    let newItem = ChecklistItemEntity(context: moc)
                    newItem.id = UUID()
                    newItem.step = jsonItem.step
                    newItem.action = jsonItem.action
                    newItem.checklist = newChecklist
                    return newItem
                }))
                return newChecklist
            }))
        })
        
        try moc.save()
    }
}
