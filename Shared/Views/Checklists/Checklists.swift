//
//  ChecklistsView.swift
//  iChecklist (iOS)
//
//  Created by Maciej Zajęcki on 24/10/2021.
//

import SwiftUI
import CoreData

struct Checklists: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: []) private var types: FetchedResults<TypeEntity>

    @State private var isEditing: Bool = false
    @State private var newChecklistCounter = 1

    
    func addChecklist() {
        let newChecklist = ChecklistEntity(context: moc)
        newChecklist.id = UUID()
        newChecklist.name = String(format: "New %d", newChecklistCounter)
        newChecklist.items = []
        newChecklist.type = types.first(where: {$0.isSelected})
        try? moc.save()
        
        newChecklistCounter += 1
    }
    
    private var AddButton: some View {
        Button(action: {addChecklist()}, label: {
            HStack {
                Spacer()
                Text("Add checklist")
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 40, maxHeight: 40, alignment: .center)
                Spacer()
            }
        })
    }
    
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    if (types.first(where: {$0.isSelected}) != nil) {
                        ChecklistsList(currentType: types.first(where: {
                            $0.isSelected
                            
                        })!, isEditing: isEditing)
                    }
                    
                    AddButton.isEmpty(!isEditing)
                }
                .navigationTitle(types.first(where: {$0.isSelected}) != nil ? "\(types.first(where: {$0.isSelected})!.name!) checklists" : "No aircraft selected")
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        NavigationLink(destination: {
                            TypeSelection(newTypeCallback: {
                                isEditing = true
                                
                            })
                                .navigationBarBackButtonHidden(true)
                            
                        }, label: {
                            Text("Aircrafts")
                        })
                            .isDetailLink(false)
                            .isEmpty(isEditing)
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action: {
                            isEditing.toggle()
                            
                        }, label: {
                            Text(isEditing ? "Done" : "Edit")
                            
                        })
                    }
                }
            }
            
            
            Welcome()
        }
        .phoneOnlyStackNavigationView()
    }
}
