//
//  ChecklistsList.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import SwiftUI

struct ChecklistsList: View {
    @Environment(\.managedObjectContext) private var moc
    @State(initialValue: nil) private var checklistIndex: Int?
    
    @ObservedObject var currentType: TypeEntity
    var isEditing: Bool
    
    func deleteChecklist(checklist: ChecklistEntity) {
        moc.delete(checklist)

        try? moc.save()
    }
    
    func markAsEmergency(checklist: ChecklistEntity) {
        checklist.isEmergency.toggle()
        
        try? moc.save()
    }
    
    var body: some View {
        ForEach((currentType.checklists?.allObjects as! [ChecklistEntity])) { checklist in
            Group {
                if (isEditing) {
                    NavigationLink(destination: {
                        ChecklistEdit(checklist: checklist)
                    }, label: {
                        ChecklistRow(checklist: checklist)
                    })
                        .isDetailLink(false)
                        .swipeActions(edge: .trailing, allowsFullSwipe: false, content: {
                            Button(role: .destructive) {
                                deleteChecklist(checklist: checklist)
                            } label: {
                                Label("Delete", systemImage: "trash")
                            }
                        })
                        .swipeActions(edge: .trailing, allowsFullSwipe: false, content: {
                            Button {
                                markAsEmergency(checklist: checklist)
                            } label: {
                                Label("Mark as emer.", systemImage: "e.square.fill")
                            }
                            .tint(.orange)
                        })
                        
                } else {
                    NavigationLink(destination: {
                        Checklist(currentChecklist: checklist)
                    }, label: {
                        ChecklistRow(checklist: checklist)
                    })
                        .isDetailLink(false)
                        .disabled(checklist.items!.count == 0)

                }
                    
            }
        }
    }
}
