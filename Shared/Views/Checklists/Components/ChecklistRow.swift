//
//  ChecklistRow.swift
//  iChecklist (iOS)
//
//  Created by Maciej Zajęcki on 24/10/2021.
//

import SwiftUI

struct ChecklistRow: View {
    @ObservedObject var checklist: ChecklistEntity
    
    var body: some View {
        HStack {
            VStack {
                HStack {
                    Text(checklist.name!)
                        .font(.title)
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                    
                }
                HStack {
                    Text("\(checklist.items!.count) items")
                        .font(.title3)
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                    Image(systemName: "e.square.fill")
                        .foregroundColor(.red)
                        .padding(.trailing, 10)
                        .isEmpty(!checklist.isEmergency)
                }
            }
            .frame(width: .infinity, height: 80, alignment: .center)
            .padding(.leading, 10)
        }
    }
}
