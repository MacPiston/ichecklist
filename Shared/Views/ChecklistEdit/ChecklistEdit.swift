//
//  ChecklistView.swift
//  iChecklist
//
//  Created by Maciej Zajęcki on 16/10/2021.
//

import SwiftUI
import CoreData

struct ChecklistEdit: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) private var moc
    @State private var isShowingNewTaskRow: Bool = false
    @State private var newName: String = ""
    
    let checklist: ChecklistEntity

    func handleSave() {
        try? moc.save()
        presentationMode.wrappedValue.dismiss()
    }
    
    func handleDelete(at offsets: IndexSet) {
        for index in offsets {
            let item = (checklist.items!.allObjects as! [ChecklistItemEntity])[index]
            print(item)
            moc.delete(item)
        }
        try? moc.save()
    }
    
    func onSaveSucceeded() {
        isShowingNewTaskRow = false
    }
    
    func createNewItem() {
        let newItem = ChecklistItemEntity(context: moc)
        newItem.id = UUID()
        newItem.step = "New step"
        newItem.action = ""
        newItem.checklist = checklist
        
        try? moc.save()
    }
    
    func changeName() {
        if newName != "" {
            checklist.name = newName
            newName = ""
            
            try? moc.save()
        }
    }
    
    var body: some View {
        
            List {
                Tasks(checklist: checklist, onDelete: handleDelete)
                
                
                Button(action: createNewItem, label: {
                    HStack {
                        Spacer()
                        Text("Add step")
                            .frame(width: .infinity, height: 40, alignment: .center)
                        Spacer()
                    }
                })
            }
            .navigationTitle(checklist.name!)
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: handleSave, label: {
                        Text("Save")
                    })
                }
                ToolbarItem(placement: .bottomBar) {
                    HStack(alignment: .center) {
                        TextField("New name...", text: $newName)
                        Button(action: { changeName() }, label: {
                            Text("Save name")
                        })
                            .disabled(newName == "")
                    }
                    .padding([.leading, .trailing], 30)
                }
            }
            
            
    }
}
