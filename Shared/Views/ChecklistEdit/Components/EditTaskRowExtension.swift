//
//  EditTaskRowExtension.swift
//  iChecklist
//
//  Created by Maciej Zajęcki on 16/10/2021.
//
import Combine
import SwiftUI

struct EditTaskRowExtension: View {
    @State private var validationModel: EditTaskRowValidationModel
    private let checklistItem: ChecklistItemEntity
    
    private let componentMaxHeight: CGFloat = 250
    private let textFieldMaxWidth: CGFloat = 200
    private let textFieldsSpacing: CGFloat = 30
    
    init(item: ChecklistItemEntity) {
        self.checklistItem = item
        self._validationModel = State(initialValue: EditTaskRowValidationModel(step: item.step!, action: item.action!))
    }
    
    func handleSave() {
        checklistItem.step = validationModel.step
        checklistItem.action = validationModel.action
    }
    
    var body: some View {
        VStack {
            HStack(alignment: .center) {
                VStack {
                    Text("Step")
                        .fontWeight(.semibold)
                    TextField("Step", text: $validationModel.step)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .multilineTextAlignment(.center)
                        .frame(minWidth: 40, maxWidth: textFieldMaxWidth, minHeight: 20, maxHeight: 20, alignment: .center)
                }
                .frame(minWidth: 40, maxWidth: .infinity, minHeight: 50, maxHeight: 50, alignment: .center)
                
                VStack {
                    Text("Action")
                        .fontWeight(.semibold)
                    TextField("Action", text: $validationModel.action)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .multilineTextAlignment(.center)
                        .frame(minWidth: 40, maxWidth: textFieldMaxWidth, minHeight: 20, maxHeight: 20, alignment: .center)
                }
                .frame(minWidth: 40, maxWidth: .infinity, minHeight: 60, maxHeight: .infinity, alignment: .center)
            }
            .padding(.top, 8)
            
            Button(action: handleSave, label: {Text("Save")})
                .disabled(!validationModel.saveDisabled)
                .padding(.top, 8)
                .padding(.bottom, 8)
        }
        .frame(minWidth: 100, maxWidth: .infinity, minHeight: 80, maxHeight: componentMaxHeight, alignment: .center)
    }
    
}
