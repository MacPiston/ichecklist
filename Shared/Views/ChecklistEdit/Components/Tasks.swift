//
//  Tasks.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import SwiftUI

struct Tasks: View {
    @ObservedObject var checklist: ChecklistEntity
    var onDelete: (_ offsets: IndexSet) -> Void
    
    var body: some View {
        ForEach(Array(checklist.items! as Set).reversed(), id: \.self) { item in
            SingleTaskRow(checklistItem: item as! ChecklistItemEntity)
        }
        .onDelete(perform: onDelete)
    }
}
