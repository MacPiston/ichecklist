//
//  ChecklistItemView.swift
//  iChecklist
//
//  Created by Maciej Zajęcki on 16/10/2021.
//

import SwiftUI

struct SingleTaskRow: View {
    @State var isEditing: Bool
    
    private var checklistItem: ChecklistItemEntity
    
    init(checklistItem: ChecklistItemEntity) {
        self.checklistItem = checklistItem
        self._isEditing = State(initialValue: checklistItem.action! == "")
    }
    
    var Title: some View {
        Text(checklistItem.step!)
            .font(.system(size: 24))
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    var EditArrow: some View {
        Button(action: {
            isEditing.toggle()
        }, label: {
            Image(systemName: isEditing ? "chevron.compact.down" : "chevron.compact.up")
                .font(.system(size: 24))
        })
        
    }
    
    var body: some View {
        VStack {
            HStack(alignment: .center) {
                Title
                    .padding(.leading, 40)
                EditArrow
                    .padding(.trailing, 20)
            }
            .frame(minWidth: 100, maxWidth: .infinity, minHeight: 40, maxHeight: 40, alignment: .center)
            EditTaskRowExtension(item: checklistItem).isEmpty(!isEditing)
        }
    }
}
