//
//  EditTaskRowExtensionModel.swift
//  iChecklist
//
//  Created by Maciej Zajęcki on 24/10/2021.
//

import Foundation

class EditTaskRowValidationModel: ObservableObject {
    private let initialStep: String
    private let initialAction: String
    
    @Published var step: String {
        didSet {
            validate()
        }
    }
    @Published var action: String {
        didSet {
            validate()
        }
    }
    @Published var saveDisabled: Bool = true
    
    func validate() {
        if (!step.isEmpty && !action.isEmpty && (step != initialStep || action != initialAction)) {
            saveDisabled = false
        } else {
            saveDisabled = true
        }
    }
    
    init(step: String, action: String) {
        self.initialStep = step
        self.initialAction = action
        self.step = step
        self.action = action
    }
}
