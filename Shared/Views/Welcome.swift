//
//  Welcome.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import SwiftUI

struct Welcome: View {
    var body: some View {
        VStack(spacing: 18) {
            Text("Welcome to iChecklist!")
                .font(.system(size: 48))
            
            VStack(spacing: 10) {
                Text("1. Add aircraft type using \"Aircrafts\" button in top left corner")
                Text("2. Create new checklist using \"Add new checklist button\"")
                Text("3. Add steps to the checklist and save")
                Text("4. Save editing, tap on selected checklist and you are ready to go! ✈️")
            }
        }
    }
}

struct Welcome_Previews: PreviewProvider {
    static var previews: some View {
        Welcome()
    }
}
