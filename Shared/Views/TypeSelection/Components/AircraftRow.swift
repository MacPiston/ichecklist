//
//  AircraftRow.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import SwiftUI

struct AircraftRow: View {
    private let name: String
    private let isChecked: Bool
    
    init(name: String, isChecked: Bool) {
        self.name = name
        self.isChecked = isChecked
    }
    
    var body: some View {
        HStack {
            Text(name)
                .frame(maxWidth: .infinity, alignment: .leading)
            if (isChecked) {
                Image(systemName: "checkmark")
                    .frame(alignment: .trailing)
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
    }
}

struct AircraftRow_Previews: PreviewProvider {
    static var previews: some View {
        AircraftRow(name: "test", isChecked: true)
    }
}
