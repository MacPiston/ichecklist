//
//  AircraftSelection.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import SwiftUI
import CoreData
import UniformTypeIdentifiers

struct TypeSelection: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: []) var types: FetchedResults<TypeEntity>
    @State private var showingNewAircraft = false
    @State private var fileAlertShown: Bool = false
    @State private var fileImportShown: Bool = false
    
    private var newTypeCallback: () -> Void
    private let exportProvider: JSONExportProvider = JSONExportProvider()
    private let importProvider: JSONImportProvider = JSONImportProvider()

    init(newTypeCallback: @escaping () -> Void) {
        self.newTypeCallback = newTypeCallback
    }
    
    
    func setCurrentType(id: UUID) {
        types.forEach({$0.isSelected = false})
        let newCurrent = types.first(where: {$0.id == id})
        newCurrent?.isSelected = true
        try? moc.save()
        
        presentationMode.wrappedValue.dismiss()
    }
    
    func createNewType(name: String) {
        let newType = TypeEntity(context: moc)
        newType.id = UUID()
        newType.name = name
        newType.checklists = []
        newType.isSelected = true
        
        types.forEach({$0.isSelected = false})
        
        try? moc.save()
        
        presentationMode.wrappedValue.dismiss()
        newTypeCallback()
    }
    
    func deleteType(at offsets: IndexSet) {
        for index in offsets {
            let type = types[index]
            moc.delete(type)
        }
        try? moc.save()
    }
    
    var body: some View {
        List {
            ForEach(types) { type in
                Button(action: {setCurrentType(id: type.id!)}, label: {
                    AircraftRow(name: type.name!, isChecked: type.isSelected)
                })
            }
            .onDelete(perform: deleteType)
        }
        .alert(isPresented: $showingNewAircraft, TextAlert(title: "New type", message: "Enter new aircraft's type", keyboardType: .default) { result in
            if let name = result {
                createNewType(name: name)
            }
        })
        .alert("Import/export", isPresented: $fileAlertShown, actions: {
            Button("Import") { fileImportShown = true }
            Button("Export") { exportProvider.exportToJSON(data: Array(types)) }
            Button("Cancel", role: .cancel) { }
        })
        .navigationTitle(types.first(where: {$0.isSelected}) != nil ? "Select type" : "Add aircraft type")
        .toolbar {
            ToolbarItem {
                Button(action: {showingNewAircraft = true}, label: {
                    Image(systemName: "plus")
                })
            }
            ToolbarItem(placement: .navigationBarLeading) {
                Button(action: {
                    fileAlertShown = true
                }, label: {
                    Text("Import/export")
                })
            }
        }
        .fileImporter(isPresented: $fileImportShown, allowedContentTypes: UTType.types(tag: "json", tagClass: .filenameExtension, conformingTo: nil), allowsMultipleSelection: false, onCompletion: { result in
            if case .success = result {
                do {
                    let fileUrl = try result.get().first
                    try importProvider.importJson(fileUrl: fileUrl!, moc: moc)
                } catch {
                    print("Error during import")
                }
            }
        })
    }
}

struct TypeSelection_Previews: PreviewProvider {
    static var previews: some View {
        TypeSelection(newTypeCallback: {})
    }
}
