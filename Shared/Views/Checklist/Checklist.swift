//
//  Checklist.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import SwiftUI
import AVFoundation

struct Checklist: View {
    //    let _ttsProvider = TTSProvider()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @FetchRequest(entity: ChecklistEntity.entity(), sortDescriptors: []) private var emergencyChecklists: FetchedResults<ChecklistEntity>
    
    @State(initialValue: 0) private var currentItemIndex: Int
    @State(initialValue: true) private var soundEnabled: Bool
    @State(initialValue: false) private var finalAlertVisible: Bool
    @State(initialValue: false) private var emergencyAlertVisible: Bool
    @State(initialValue: nil) private var emergencyChecklist: ChecklistEntity?
    @State(initialValue: false) private var emergencyChecklistDisplayed: Bool

    private var currentChecklist: ChecklistEntity
    private var asEmergency: Bool
    
    init(currentChecklist: ChecklistEntity, asEmergency: Bool? = false) {
        self.currentChecklist = currentChecklist
        self.asEmergency = asEmergency!
        _emergencyChecklists = FetchRequest(sortDescriptors: [], predicate: NSPredicate(format: "%K == %@", "type.id", String(self.currentChecklist.type!.id!.uuidString)))
    }
    
    func handlePrev() {
        if (self.currentItemIndex - 1 >= 0) {
            self.currentItemIndex -= 1
        }
    }
    
    func handleNext() {
        if (self.currentItemIndex + 1 < currentChecklist.items!.count) {
            self.currentItemIndex += 1
        } else {
            finalAlertVisible = true
        }
    }
    
    var body: some View {
        VStack(spacing: 0) {
            CurrentItem(item: (self.currentChecklist.items!.allObjects as! [ChecklistItemEntity])[self.currentItemIndex])
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
                .background(asEmergency ? Color(hex: "#f57b42") : Color(.white))
            
            Navigation(onPrev: handlePrev, onNext: handleNext)
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 120, alignment: .center)
                .padding(.bottom, 20)
                .background(asEmergency ? Color(hex: "#f57b42") : Color(.white))
            
            if (!asEmergency) {
                Emergency(onEmergencyPressed: { emergencyAlertVisible = true })
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 120, alignment: .center)
                    .padding([.leading, .trailing], 10)
                
                    NavigationLink(isActive: $emergencyChecklistDisplayed, destination: {
                        Group {
                            if (emergencyChecklistDisplayed) {
                                Checklist(currentChecklist: emergencyChecklist!, asEmergency: true)
                            } else {
                                EmptyView()
                            }
                        }
                        
                    }, label: {
                        EmptyView()
                        
                    })
            }
        }
        .alert("Checklist finished", isPresented: $finalAlertVisible, actions: {
            Button("Return to list", action: {
                presentationMode.wrappedValue.dismiss()
            })
            Button("Dismiss", role: .cancel, action: {})
        }, message: {
            Text("This was the last step")
        })
        .alert("Emergency checklists", isPresented: $emergencyAlertVisible, actions: {
            ForEach(emergencyChecklists) { emChecklist in
                Button(emChecklist.name!, role: .destructive) {
                    emergencyChecklist = emChecklist
                    emergencyChecklistDisplayed = true
                }
            }
            Button("Cancel", role: .cancel) { }
        })
        .navigationTitle(currentChecklist.name!)
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: {self.soundEnabled.toggle()}, label: {
                    Image(systemName: self.soundEnabled ? "speaker.wave.2" : "speaker.slash")
                })
            }
        }
    }
}
