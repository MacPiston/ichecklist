//
//  CurrentItem.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import SwiftUI

struct CurrentItem: View {
    private let step: String
    private let action: String
    
    init(item: ChecklistItemEntity) {
        self.step = item.step!
        self.action = item.action!
    }
    
    var body: some View {
        VStack(spacing: 0) {
            Text(self.step)
                .font(.system(size: 65.0))
            
            Image(systemName: "arrowtriangle.down.fill")
                .font(.system(size: 100.0))
                .padding(.bottom, 30)
                .padding(.top, 30)
            
            Text(self.action)
                .font(.system(size: 65.0))
        }
    }
}
