//
//  Navigation.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import SwiftUI

struct Navigation: View {
    private var onPrev: () -> Void
    private var onNext: () -> Void
    
    init(onPrev: @escaping () -> Void, onNext: @escaping () -> Void) {
        self.onPrev = onPrev
        self.onNext = onNext
    }
    
    var body: some View {
        GeometryReader { metrics in
            HStack(spacing: 0) {
                Button(action: self.onPrev, label: {
                    Image(systemName: "chevron.backward")
                        .font(.system(size: 50.0))
                        .foregroundColor(Color.black)
                })
                    .frame(width: metrics.size.width * 0.30, height: metrics.size.height)
                    .background(Color(hex: "#b2b0b5"))
                    .cornerRadius(25)
                    .overlay(RoundedRectangle(cornerRadius: 25).stroke(Color(hex: "#8e8d91"), lineWidth: 4).padding(2))
                    
                
                Button(action: self.onNext, label: {
                    Image(systemName: "checkmark")
                        .font(.system(size: 80.0))
                        .foregroundColor(Color.black)
                })
                    .frame(width: metrics.size.width * 0.60, height: metrics.size.height)
                    .background(Color(hex: "#80d266"))
                    .cornerRadius(25)
                    .overlay(RoundedRectangle(cornerRadius: 25).stroke(Color(hex: "#49be25"), lineWidth: 4).padding(2))
                    .padding(.leading, metrics.size.width * 0.02)
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
        }
        
    }
}

struct Navigation_Previews: PreviewProvider {
    static var previews: some View {
        Navigation(onPrev: {}, onNext: {})
    }
}
