//
//  Emergency.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import SwiftUI

struct Emergency: View {
    var onEmergencyPressed: () -> Void;

    var body: some View {
        Button(action: { onEmergencyPressed() }, label: {
            Text("Emergency")
                .foregroundColor(Color.black).font(.system(size: 50.0))
            
        })
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
            .background(Color(hex: "#ff2a00"))
            .cornerRadius(25)
            .overlay(RoundedRectangle(cornerRadius: 25).stroke(Color(hex: "#cc2200"), lineWidth: 4).padding(2))    }
    
}

struct Emergency_Previews: PreviewProvider {
    static var previews: some View {
        Emergency(onEmergencyPressed: {})
    }
}
