//
//  Data.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import Foundation

func getDocumentsDirectory() -> NSString {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory as NSString
}

extension Data {
    func dataToFile(fileName: String) -> NSURL? {

        let data = self

        let filePath = getDocumentsDirectory().appendingPathComponent(fileName)

        do {
            try data.write(to: URL(fileURLWithPath: filePath))

            return NSURL(fileURLWithPath: filePath)

        } catch {
            print("Error writing the file: \(error.localizedDescription)")
        }

        return nil
    }
}
