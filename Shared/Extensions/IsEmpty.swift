//
//  IsEmpty.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 25/12/2021.
//

import Foundation
import SwiftUI

struct EmptyModifier: ViewModifier {
    let isEmpty: Bool
    
    func body(content: Content) -> some View {
        Group {
            if isEmpty {
                EmptyView()
            } else {
                content
            }
        }
    }
}

extension View {
    func isEmpty(_ bool: Bool) -> some View {
        modifier(EmptyModifier(isEmpty: bool))
    }
}
