//
//  View.swift
//  iChecklist
//
//  Created by Maciej Zajecki on 26/12/2021.
//

import Foundation
import SwiftUI

extension View {
    func phoneOnlyStackNavigationView() -> some View {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return AnyView(self.navigationViewStyle(.stack))
        } else {
            return AnyView(self)
        }
    }
}
